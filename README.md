*Awesome Plugin*

Awesome plugin shows you how to use internationalisation in your JavaScript for an Atlassian product add-on.

* It uses JavaScript to append "is awesome!" to the end of every user's name in Confluence.
* It is fully internationalised in the following languages
  - English
  - American English
  - French
  - German
  - Spanish