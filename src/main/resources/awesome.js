
function makeUsersAwesome(text) {
    AJS.$('a.confluence-userlink').each(function () {
        // Append *text* to every user link
        AJS.$(this).text(AJS.$(this).text() + ' ' + text); 
    });
    var $userMenuLink = AJS.$('#user-menu-link span span:first');
    $userMenuLink.text($userMenuLink.text() + ' ' + text);
}

AJS.toInit(function () {

    AJS.log(' +++ awesome-js +++ ');

    AJS.I18n.get('me.davidsimpson.confluence.plugins.awesome-plugin',
        function(){ // success
            makeUsersAwesome(AJS.I18n.keys['awesome-plugin.user.is.awesome']);
         },
        function(){ // error
            makeUsersAwesome(' is awesome'); // fallback to default text
        }
    ); // Read the i18n from engagement.properties
});
